#! /usr/bin/python
from __future__ import print_function
import argparse
import json
import os
import ovh
import socket
from six.moves.urllib.request import urlopen

def read_json_file(ffile):
    with open(ffile, "r") as f:
        d = json.load(f)
    return d

def get_options():
    parser = argparse.ArgumentParser()
    parser.add_argument("--zonename", default="villard.me")
    parser.add_argument("--target", default=urlopen("http://ip.42.pl/raw").read().decode("utf-8"))
    parser.add_argument("--subdomain", default="home")
    parser.add_argument("--hostname", help=argparse.SUPPRESS, default=socket.gethostname())
    parser.add_argument("--ovh-secret", dest="ovh_secret", help="The file with connectivity to OVH account", default="ovh.secret")
    return parser

def get_client(args):
    d = read_json_file(args.ovh_secret)
    return ovh.Client(
        endpoint=d["endpoint"],
        application_key=d["application_key"],
        application_secret=d["application_secret"],
        consumer_key=d["consumer_key"]
    )

def ip2dns(args):
    client = get_client(args)
    try:
        iid = (client.get("/domain/zone/{args.zonename}/record".format(args=args), subDomain=args.subdomain))[0]
        target = client.get("/domain/zone/{args.zonename}/record/{iid}".format(args=args, iid=iid))["target"]
        if not target == args.target:
            # Update
            client.put("/domain/zone/{args.zonename}/record/{iid}".format(args=args, iid=iid), subDomain=args.subdomain, target=args.target)
            print("DNS updated")
        else:
            print("no update")
    except IndexError:
        # Create
        client.post('/domain/zone/{args.zonename}/record'.format(args=args), 
            fieldType='A',
            subDomain=args.subdomain,
            target=args.target
        )
        print("DNS created")

if __name__ == "__main__":
    args = get_options().parse_args()
    print(args)
    ip2dns(args)