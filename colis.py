#! /usr/bin/python
"""
Usage:
    colis.py

A tool to help writing mail to Amadeus Bel-Air conciergery for coming packages.

"""
import subprocess
try:
    import docopt
except ImportError:
    subprocess.check_call(["python", "-m", "pip", "install", "docopt"])
    import docopt
try:
    import prettytable
except ImportError:
    subprocess.check_call(["python", "-m", "pip", "install", "prettytable"])
    import prettytable

FINISH = "stop"
CANCEL = "exit"

NUMBER = "Numero de colis"
ORIGIN = "Provenance"
TRANSPORT = "Transporteur"
DATE = "Date de livraison"
REMARK = "Remarques"

params = [NUMBER, ORIGIN, TRANSPORT, DATE, REMARK]

def main(args):
    packages = []
    another_package = True
    while another_package:
        number = input("Package nb ([{FINISH}] to finish / [{CANCEL}] to cancel)? ".format(FINISH=FINISH, CANCEL=CANCEL))
        if number == FINISH:
            another_package = False
            break
        if number == CANCEL:
            exit("As aborted by user command")
        origin = input("Coming from? ")
        transport = input("Transport? ")
        delivery_date = input("Date of delivery? ")
        remark = input("Remarks? ")
        packages.append({NUMBER: number, ORIGIN: origin, TRANSPORT: transport, DATE: delivery_date, REMARK: remark})
    table = prettytable.PrettyTable()
    table.field_names = params
    [table.add_row([package[param] for param in params])for package in packages]
    print(table)  

if __name__ == "__main__":
    main(docopt.docopt(__doc__))