#! /usr/bin/python
import argparse
try:
    import clipboard
except ImportError:
    import subprocess
    subprocess.check_call(["python", "-m", "pip", "install", "-q", "--user", "clipboard"])
    import clipboard
try:
    from six.moves import input
except ImportError:
    import subprocess
    subprocess.check_call(["python", "-m", "pip", "install", "-q", "--user", "six"])
    from six.moves import input

DEFAULT_TIME = "<input>"

def type_time(time):
    if time == DEFAULT_TIME:
        return input("Please enter time (hh.mm.ss) ")
    else:
        return time

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("time", nargs="?", default=DEFAULT_TIME, type=type_time)
    args = parser.parse_args()
    items = list(map(int, args.time.split(".")))
    output = items[0]*1./24 + items[1]*1./24/60 + items[2]*1./24/60/60
    print(output)
    clipboard.copy(str(output))

if __name__ == "__main__":
    main()