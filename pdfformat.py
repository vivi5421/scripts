#! /usr/bin/python3
from __future__ import print_function
import argparse
import copy
import itertools
import logging
import math
import os
import re
import shutil
import subprocess
import tempfile
import time
import traceback

try:
    import PyPDF2
except ImportError:
    subprocess.check_call(["python", "-m", "pip", "install", "-q", "--user", "PyPDF2"])
    import PyPDF2


EXIT = "action_exit" # Is the only EXIT code allowed to leave the program. Any other exit code will be excepted.

def action_add(**kwargs):
    for ffile in kwargs["args"].files:
        pdf_file = PyPDF2.PdfFileReader(open(ffile, "rb"))
        [kwargs["pages"].append(page) for page in pdf_file.pages]

def action_rotate(**kwargs):
    [kwargs["pages"][index].rotateClockwise(kwargs["args"].angle) for index in kwargs["args"].pages]

def action_move(**kwargs):
    """
    >>> pages = [0, 1]
    >>> action_move(**{"pages": pages, "args": actions_parser(pages).parse_args(["move", "1", "2"])})
    >>> pages
    [1, 0]
    >>> pages = [0, 1, 2, 3, 4, 5]
    >>> action_move(**{"pages": pages, "args": actions_parser(pages).parse_args(["move", "1", "reverse(odd(1-6))"])})
    >>> pages
    [4, 2, 0, 1, 3, 5]
    """
    extract = [kwargs["pages"][x] for x in kwargs["args"].pages]
    [kwargs["pages"].remove(x) for x in extract]
    initial_rank = kwargs["args"].destination - len([x for x in kwargs["args"].pages if x < kwargs["args"].destination])
    [kwargs["pages"].insert(initial_rank, obj) for obj in reversed(extract)]

def vertical_split(pdf_page):
        # Source: https://stackoverflow.com/questions/13345593/split-each-pdf-page-in-two
        p = copy.copy(pdf_page)
        q = copy.copy(pdf_page)
        p.mediaBox = copy.copy(p.cropBox)
        q.mediaBox = copy.copy(p.cropBox)
        x1, x2 = p.mediaBox.lowerLeft
        x3, x4 = p.mediaBox.upperRight
        x1, x2 = math.floor(x1), math.floor(x2)
        x3, x4 = math.floor(x3), math.floor(x4)
        x6 = x2+math.floor((x4-x2)/2)
        p.mediaBox.upperRight = (x3, x4)
        p.mediaBox.lowerLeft = (x1, x6)
        q.mediaBox.upperRight = (x3, x6)
        q.mediaBox.lowerLeft = (x1, x2)
        p.artBox = p.mediaBox
        q.artBox = q.mediaBox
        p.bleedBox = p.mediaBox
        q.bleedBox = q.mediaBox
        p.cropBox = p.mediaBox
        q.cropBox = q.mediaBox
        return p,q

def action_split(**kwargs):
    for page_index in sorted(kwargs["args"].pages, reverse=True):
        page_left, page_right = vertical_split(kwargs["pages"][page_index])
        kwargs["pages"].remove(kwargs["pages"][page_index])
        kwargs["pages"].insert(page_index, page_left)
        kwargs["pages"].insert(page_index + 1, page_right)

def action_delete(**kwargs):
    """
    >>> pages = [1, 2, 3, 4]
    >>> action_delete(**{"pages": pages, "args": actions_parser(pages).parse_args(["delete", "2"])})
    >>> pages
    [1, 3, 4]
    """
    [kwargs["pages"].remove(kwargs["pages"][page_index]) for page_index in sorted(kwargs["args"].pages, reverse=True)]

def action_count(**kwargs):
    """
    >>> pages = [1, 2, 3, 4]
    >>> action_count(**{"pages": pages, "args": actions_parser(pages).parse_args(["count"])})
    Total: 4 pages.
    """
    print("Total: {} pages.".format(len(kwargs["pages"])))

def save_pdf_to_file(pdf_pages, filename):
    output_pdf = PyPDF2.PdfFileWriter()
    [output_pdf.addPage(page) for page in pdf_pages]
    with open(filename, "wb") as outputStream:
        output_pdf.write(outputStream)

def action_save(**kwargs):
    save_pdf_to_file([kwargs["pages"][index] for index in kwargs["args"].pages], kwargs["args"].filename)

def action_preview(**kwargs):
    # pylint: disable=E1101
    # Module 'tempfile' has no 'TemporaryDirectory' member
    temp_dir = tempfile.mkdtemp()
    try:
        with tempfile.NamedTemporaryFile() as tf:
            name = os.path.basename(tf.name)
        temp_file = os.path.join(temp_dir, "{}.pdf".format(name))
        temp_file = 'toto.pdf'
        save_pdf_to_file(kwargs["pages"], temp_file)
        try:
            subprocess.check_call("start {}".format(temp_file), shell=True)
        except:
            #os.system("xdg-open {}".format(temp_file))
            traceback.print_tb
        time.sleep(5)
    finally:
        shutil.rmtree(temp_dir)

def action_exit(**kwargs):
    exit(EXIT)

def tpage(page):
    """
    >>> tpage("5")
    4
    >>> tpage("5-10")
    Traceback (most recent call last):
        ...
    argparse.ArgumentTypeError: '5-10' is not a valid number
    """
    if re.match(r"^\d+$", page):
        return int(page) - 1
    raise argparse.ArgumentTypeError("'{}' is not a valid number".format(page))

RANGE_OPERATIONS = [] # "Useless" initialization because RANGE_OPERATIONS requires tpages method but tpages method requires RANGE_OPERATIONS

def expand(pages):
    """
    >>> expand("reverse(1-5);odd(3-8);reverse(even(3-8));5-1")
    [5, 4, 3, 2, 1, 3, 5, 7, 8, 6, 4]
    """
    output = []
    items = pages.split(";")
    for item in items:
        # We assume only one range_operations will be returned
        operation = [op for op in RANGE_OPERATIONS if re.match(op["regexp"], item)][0]
        [output.append(x) for x in operation["method"](re.match(operation["regexp"], item).groups())]
    return output

def tpages(pages):
    """
    >>> tpages("reverse(1-5);odd(3-8);reverse(even(3-8));5-1")
    [4, 3, 2, 1, 0, 2, 4, 6, 7, 5, 3]
    """
    return [x - 1 for x in expand(pages)]

def list_feuillet(llist):
    """
    >>> list_feuillet([1, 2, 3, 4, 5, 6, 7, 8])
    [6, 3, 8, 1, 2, 7, 4, 5]
    >>> list_feuillet(range(1, 13))
    [8, 5, 10, 3, 12, 1, 2, 11, 4, 9, 6, 7]
    """
    n = len(llist)
    mid=int(n/2)
    return list(itertools.chain(*[((mid+1+2*i)%n+1, (mid-2-2*i)%n+1) for i in range(mid)]))

RANGE_OPERATIONS.append({"regexp": r"^reverse\((.+)\)$", "method": lambda match: reversed(expand(match[0]))})
RANGE_OPERATIONS.append({"regexp": r"^odd\((.+)\)$", "method": lambda match: [x for x in expand(match[0]) if x % 2 == 1]})
RANGE_OPERATIONS.append({"regexp": r"^even\((.+)\)$", "method": lambda match: [x for x in expand(match[0]) if x % 2 == 0]})
RANGE_OPERATIONS.append({"regexp": r"^(\d+)$", "method": lambda match: [int(match[0])]})
RANGE_OPERATIONS.append({"regexp": r"^(\d+)-(\d+)$", "method": lambda match: range(int(match[0]), int(match[1]) + 1)})
RANGE_OPERATIONS.append({"regexp": r"^feuillet\((.+)\)$", "method": lambda match: list_feuillet(expand(match[0]))})

def actions_parser(pages):
    """
    >>> args = actions_parser([1, 2, 3, 4, 5]).parse_args(["rotate", "180", "2-5"])
    >>> args.subparser, args.pages, args.angle
    ('rotate', [1, 2, 3, 4], 180)
    >>> args = actions_parser([1, 2, 3, 4, 5]).parse_args(["save", "toto.pdf", "reverse(odd(1-5))"])
    >>> args.subparser, args.pages, args.filename
    ('save', [4, 2, 0], 'toto.pdf')
    """
    all_pages = range(1, len(pages) + 1)
    # Main parser
    parser = argparse.ArgumentParser(description="Please enter the action you want to execute", prog="", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    subparsers = parser.add_subparsers(dest="subparser")
    # Add parser
    parser_add = subparsers.add_parser('add', help='add PDF files', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser_add.add_argument("files", help="PDF files to be added", nargs="*")
    parser_add.set_defaults(action=action_add)
    # Rotate parser
    parser_rotate = subparsers.add_parser('rotate', help="rotate some pages", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser_rotate.add_argument('angle', help="the angle (clockwise)", choices=[90, 180, 270], type=int)
    parser_rotate.add_argument('pages', help="the impacted pages", type=tpages, default=all_pages)
    parser_rotate.set_defaults(action=action_rotate)
    # Move parser
    parser_move = subparsers.add_parser('move', help="move some pages", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser_move.add_argument('destination', help="the first rank of destination", type=tpage)
    parser_move.add_argument('pages', help="the impacted pages", type=tpages)
    parser_move.set_defaults(action=action_move)
    # Split parser
    parser_split = subparsers.add_parser('split', help='split some pages vertically', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser_split.add_argument('pages', help="the impacted pages", type=tpages, default=all_pages)
    parser_split.set_defaults(action=action_split)
    # Delete parser
    parser_delete = subparsers.add_parser('delete', help='delete some pages', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser_delete.add_argument('pages', help="the impacted pages", type=tpages)
    parser_delete.set_defaults(action=action_delete)
    # Count parser
    parser_count = subparsers.add_parser('count', help='count number of available pages', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser_count.set_defaults(action=action_count)
    # Save parser
    parser_save = subparsers.add_parser('save', help='save selection of pages or the whole document', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser_save.add_argument('filename', help="file to create")
    parser_save.add_argument('pages', help="the impacted pages", type=tpages, default=all_pages)
    parser_save.set_defaults(action=action_save)
    # Preview parser
    parser_preview = subparsers.add_parser('preview', help='display PDF document in its current state', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser_preview.set_defaults(action=action_preview)
    # Exit parser
    parser_exit = subparsers.add_parser('exit', help='exit the program', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser_exit.set_defaults(action=action_exit)
    return parser

def action(pages, action):
    parser = actions_parser(pages)
    try:
        args = parser.parse_args(action)
        args.action(args=args, pages=pages, parser=parser)
    # We except SystemExit to do not exit program in case of wrong args provided to argparse
    except SystemExit as se:
        if se.code == EXIT:
            exit(0)
    except:
        traceback.print_exc()

def inputs(args):
    if args.files:
        yield ["add"] + args.files
    while True:
        yield input("action? ").split(" ")

def main(global_args):
    pages = []
    print(actions_parser(pages).print_help())
    for iinput in inputs(global_args):
        action(pages, iinput)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Modify PDF files")
    parser.add_argument("files", help="Files to be loaded", nargs="*")
    main(parser.parse_args())