#! /usr/bin/python3
from __future__ import print_function
import argparse
import re
try:
    import requests
except ImportError:
    import subprocess
    subprocess.check_call(["python", "-m", "pip", "install", "-q", "--user", "requests"])
    import requests

API_KEY = "6ca224eb-0ee6-472a-97e0-8616de6f02f8"

SVC_AIRPORTS = "https://iatacodes.org/api/v6/airports"
SVC_CITIES = "https://iatacodes.org/api/v6/cities"
SVC_COUNTRIES = "https://iatacodes.org/api/v6/countries"
SVC_AIRLINES = "https://iatacodes.org/api/v7/airlines"

def get_options():
    parser = argparse.ArgumentParser(description="Look in IATA tables")
    parser.add_argument("--airport", help="Look in Airports table", action="store_true")
    parser.add_argument("--city", help="Look in Cities table", action="store_true")
    parser.add_argument("--country", help="Look in Countries table", action="store_true")
    parser.add_argument("--airline", help="Look in Airlines table", action="store_true")
    parser.add_argument("keywords", nargs="+")
    return parser

def get_services(args):
    aall = not (args.airport or args.city or args.country or args.airline)
    services = []
    if args.airport or aall:
        services.append(SVC_AIRPORTS)
    if args.city or aall:
        services.append(SVC_CITIES)
    if args.country or aall:
        services.append(SVC_COUNTRIES)
    if args.airline or aall:
        services.append(SVC_AIRLINES)
    return services

if __name__ == "__main__":
    args = get_options().parse_args()
    keywords = "\'{}\'".format(" ".join(args.keywords).lower())
    requests.packages.urllib3.disable_warnings(requests.packages.urllib3.exceptions.InsecureRequestWarning)
    for service in get_services(args):
        results = [data for data in requests.get("{url}?api_key={API_KEY}".format(url=service, API_KEY=API_KEY), verify=False).json()["response"] if re.search(keywords, str(data).lower())]
        if results:
            print("***** {} *****".format(service.split("/")[-1].upper()))
            print("\n".join([str(result) for result in results]), end="\n\n")