#! /usr/bin/python
import argparse
import subprocess
try:
    from docopt import docopt
except ImportError:
    subprocess.check_call(["python", "-m", "pip", "install", "-q", "--user", "docopt"])
    from docopt import docopt
try:
    from prettytable import PrettyTable
except ImportError:
    subprocess.check_call(["python", "-m", "pip", "install", "-q", "--user", "prettytable"])
    from prettytable import PrettyTable
try:
    import ovh
except ImportError:
    subprocess.check_call(["python", "-m", "pip", "install", "-q", "--user", "ovh"])
    import ovh
import json
import os

def display(args, contents):
    if args.json:
        for content in contents:
            print(json.dumps(content, indent=4))
    else:
        ptable = PrettyTable()
        ptable.field_names = contents[0].keys()
        [ptable.add_row(content.values()) for content in contents]
        sortby = args.sort.replace("-", "") if args.sort else "id"
        reversesort = "-" in args.sort if args.sort else False
        print(ptable.get_string(sortby=sortby, reversesort=(reversesort)))

def action_add(args, client):
    display(args, [client.post("/email/domain/{domain}/redirection".format(domain=args.domain), _from="{name}@{domain}".format(name=name, domain=args.domain), localCopy=False, to=args.to) for name in args.names])

def action_reroute(args, client):
    data = [
        client.get("/email/domain/{domain}/redirection/{id}".format(domain=args.domain, id=iid))
        for iid in client.get("/email/domain/{domain}/redirection".format(domain=args.domain))
    ]
    display(args, [
        client.post("/email/domain/{domain}/redirection/{id}/changeRedirection".format(domain=args.domain, id=redir["id"]), to=args.new)
        for redir in data
        if redir["from"] in ["{}@{}".format(name, args.domain) for name in args.names]
    ])

def action_delete(args, client):
    data = [
        client.get("/email/domain/{domain}/redirection/{id}".format(domain=args.domain, id=iid))
        for iid in client.get("/email/domain/{domain}/redirection".format(domain=args.domain))
    ]
    display(args, [
        client.delete("/email/domain/{domain}/redirection/{id}".format(domain=args.domain, id=redir["id"]))
        for redir in data
        if redir["from"] in ["{}@{}".format(name, args.domain) for name in args.names]
    ])

def action_list(args, client):
    display(args, [
        client.get("/email/domain/{domain}/redirection/{id}".format(domain=args.domain, id=iid))
        for iid in client.get("/email/domain/{domain}/redirection".format(domain=args.domain))
    ])

def get_options():
    parser = argparse.ArgumentParser(description="A tool to manage more easily OVH mail redirections")
    parser.add_argument("-s", "--sort", help="Sort the display based on column name. Prefix with - for descending sorting")
    parser.add_argument("-j", "--json", help="JSON output", action="store_true")
    parser.add_argument("--domain", help="the domain you work on", default="villard.me")
    subparsers = parser.add_subparsers(dest="subparser")
    
    parser_add = subparsers.add_parser('create', help='create new redirections', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser_add.add_argument("to", nargs="?", default="nicolas@villard.me")
    parser_add.add_argument("names", nargs="+")
    parser_add.set_defaults(action=action_add)

    parser_reroute = subparsers.add_parser('reroute', help='Update redirections to the new address.', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser_reroute.add_argument("to")
    parser_reroute.add_argument("names", nargs="+")
    parser_reroute.set_defaults(action=action_reroute)

    parser_delete = subparsers.add_parser('delete', help='Delete some redirections', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser_delete.add_argument("names", nargs="+")
    parser_delete.set_defaults(action=action_delete)

    parser_list = subparsers.add_parser('list')
    parser_list.set_defaults(action=action_list)
    return parser

def main(args):
    print(args)
    client = ovh.Client(
        endpoint="ovh-eu",                                      # Endpoint of API OVH Europe (List of available endpoints)
        application_key="PQ6QkEyiKOlUl0Kd",                     # Application Key
        application_secret="SmSWOXwDBF3LdkcrTz9rk2ClBNZIQLlg",  # Application Secret
        consumer_key="4zPuGEeGqesVNbegvjKFKNFP3geWKbtu",        # Consumer Key
    )
    try:
        args.action(args, client)
    except AttributeError:
        pass

if __name__ == "__main__":
    main(get_options().parse_args())