#! /usr/bin/python
URL = "http://scripts.villard.me/_remotepython.py"

import sys
try:   
    import urllib.request
    exec(open(urllib.request.urlretrieve(URL)[0]).read(), globals())
except (ImportError, NameError):
    import urllib
    execfile(urllib.urlretrieve(URL)[0], globals())