import pdfformat

PDF_FILE = "test_pdfformat.pdf"
NB_PAGES = 4

def test_add_count(capsys):
    pages = []
    pdfformat.action(pages, ["add", PDF_FILE])
    pdfformat.action(pages, ["count"])
    assert capsys.readouterr().out == "Total: 4 pages.\n"

def test_add_preview(capsys):
    pages = []
    pdfformat.action(pages, ["add", PDF_FILE])
    pdfformat.action(pages, ["preview"])
    assert "PdfReadError" not in capsys.readouterr().err

def test_add_split(capsys):
    pages = []
    pdfformat.action(pages, ["add", PDF_FILE])
    pdfformat.action(pages, ["split", "1-4"])
    pdfformat.action(pages, ["count"])
    assert capsys.readouterr().out == "Total: 8 pages.\n"
    pdfformat.action(pages, ["preview"])
    assert "PdfReadError" not in capsys.readouterr().err