import importlib
import os
import prettytable
import subprocess

ffiles = [ffile for ffile in os.listdir(os.getcwd()) if ffile.endswith(".py") and not ffile.endswith("remotepython.py") and not ffile == "list.py" and not ffile == "list2.py"]

ptable = prettytable.PrettyTable(hrules=prettytable.ALL)
ptable.field_names = ["script", "--help"]
ptable.align = "l"
[ptable.add_row([ffile.replace(".py", ""), subprocess.check_output("python {} -h".format(ffile), shell=True).decode("utf-8")]) for ffile in ffiles]
print(ptable)