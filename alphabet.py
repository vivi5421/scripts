#! /usr/bin/python
import argparse
import itertools

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Simply give the rank of each letter", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("letters", help="letter to map", nargs="*", default=[chr(x) for x in range(ord("A"), ord("Z") + 1)], type=list)
    print("\n".join(["{}\t{}".format(letter, str(ord(letter.upper()) - ord("A") + 1)) for letter in itertools.chain(*parser.parse_args().letters)]))