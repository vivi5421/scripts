import argparse

try:
    import gitlab
except ImportError:
    import subprocess
    subprocess.check_call(["python", "-m", "pip", "install", "-q", "--user", "python-gitlab"])
    import gitlab

def get_options():
    parser = argparse.ArgumentParser(description="Modify PDF files")
    parser.add_argument("--gitlab_server", help=argparse.SUPPRESS, default="https://gitlab.com")
    parser.add_argument("--gitlab_token", help=argparse.SUPPRESS, default="JPAX4bPUv5Gy2wAAGo4_")
    parser.add_argument("--gitlab_repository", help=argparse.SUPPRESS, default="todo")
    parser.add_argument("--list-labels", dest="list_labels", help="list availables labels", action="store_true")
    parser.add_argument("title", help="title", nargs="+")
    return parser

if __name__ == "__main__":
    args = get_options().parse_args()

    # Connexion to GitLab
    gl = gitlab.Gitlab(args.gitlab_server, private_token=args.gitlab_token)
    #gl.auth()

    # Get the project
    project = [project for project in gl.projects.list(owned=True, all=True) if project.name == args.gitlab_repository][0]
    if project is None:
        exit("Project {} not found".format(args.repository))

    # Create the issue
    issue = project.issues.create({'title': " ".join(args.title), 'description': ''})
    issue.labels = ["To Do"]
    issue.save()
    print("Issue {} has been created".format(issue.iid))