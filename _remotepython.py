#! /usr/bin/python
import argparse
import itertools
import re
import sys
try:
    from six.moves import input
    from six import exec_
    from six.moves.urllib.request import urlretrieve
except ImportError:
    import subprocess
    subprocess.check_call(["python", "-m", "pip", "install", "-q", "--user", "six"])
    from six.moves import input
    from six import exec_
    from six.moves.urllib.request import urlretrieve

def get_params(args):
    if args.params:
        for arg in args.params:
            yield input("{}? ".format(arg[:-1])) if arg.endswith("?") else arg
    else:
        while True:
            param = input("param (Empty for end)? ")
            if not param:
                break
            yield param

def get_options():
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--interactive", action="store_true", default=False)
    parser.add_argument("-n", "--no_param", action="store_true", default=False)
    parser.add_argument("script", nargs="?")
    parser.add_argument("params", nargs="*", default=None)
    return parser

if __name__ == "__main__":
    is_interactive = False
    try:
        args = get_options().parse_args()
        print(args)
        is_interactive = args.interactive
        url = "https://gitlab.com/vivi5421/scripts/raw/master/{}.py".format(args.script if args.script else input("script? "))
        script_file, _ = urlretrieve(url)
        sys.argv = [url]
        if not args.no_param:
            sys.argv += list(get_params(args))
        exec(open(script_file).read(), globals())
    except:
        print(sys.exc_info()[0])
    if is_interactive:
        input("Press any key to exit...")
